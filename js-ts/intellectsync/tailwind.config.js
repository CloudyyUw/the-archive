/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
    plugins: [require('daisyui')],
    daisyui: {
        themes: [
            {
                light: {
                    primary: '#6d56d7',
                    secondary: '#e5c9f2',
                    accent: '#932ec2',
                    neutral: '#0f1f3d',
                    'base-100': '#fbfcfe',
                },
            },
        ],
    },
};

// dark prototype
// dark: {
//     primary: '#3f28a9',
//     secondary: '#290d35',
//     accent: '#a23dd1',
//     neutral: '#0f1f3d',
//     'base-100': '#010204',
// },
