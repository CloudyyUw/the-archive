import { Component, For, Show, createSignal, lazy } from 'solid-js';
import { useParams } from '@solidjs/router';
import { parseISO, format, parse } from 'date-fns';
import pocketbase from './utils/dbConnection';
import { RecordModel } from 'pocketbase';
import markdownToHtml from './utils/markdownToHtml';

import Nav from './components/nav';
import { IoWarningOutline, IoClose } from 'solid-icons/io';
import { FaSolidPlus } from 'solid-icons/fa';
import { HiOutlineTrash } from 'solid-icons/hi';
import { VsSaveAs } from 'solid-icons/vs';
import { TbCrown } from 'solid-icons/tb';
import { BiRegularPencil } from 'solid-icons/bi';

import './css/markdown.css';

const MultiSelectUsers = lazy(() => import('./components/multiSelectUsers'));

const Boards: Component = () => {
    if (pocketbase.authStore.token.length > 0 == false) {
        location.href = '/login';
    }

    const { id } = useParams(),
        [isLoading, setLoading] = createSignal(true),
        [boardData, setBoardData] = createSignal<RecordModel>(),
        [boardPosts, setBoardPosts] = createSignal([]),
        [editBoardName, setEditBoardName] = createSignal(''),
        [editBoardDescription, setEditBoardDescription] = createSignal(''),
        [editMode, setEditMode] = createSignal(false),
        [getMultiSelectItems, setMultiSelectItems] = createSignal([]),
        [userList, setUserList] = createSignal([]),
        [isUserPopupOpen, setUserPopupStatus] = createSignal(false);

    function add_user_open() {
        pocketbase
            .collection('users')
            .getFullList({
                filter: `(id != '${pocketbase.authStore.model.id}' ${boardData()
                    .members.map((m) => `&& id != '${m}'`)
                    .join(' ')})`,
            })
            .then((users) => {
                setUserList(
                    users.map((user) => {
                        return {
                            id: user.id,
                            username: user.username,
                            avatar:
                                pocketbase.files.getUrl(user, user.avatar) ||
                                '/default.jpg',
                        };
                    })
                );
                setUserPopupStatus(true);
            });
    }
    async function add_user_save() {
        try {
            await pocketbase.collection('boards').update(id, {
                members: boardData().members.concat(
                    getMultiSelectItems().map((u) => u.id) || []
                ),
            });

            location.reload();
        } catch (err) {
            console.log(err);
            alert('Failed apply changes');
        }
    }

    async function remove_user(user_id: string) {
        if (confirm('Are you sure you want to remove this user?')) {
            try {
                await pocketbase.collection('boards').update(id, {
                    members: boardData().members.filter(
                        (id: string) => id !== user_id
                    ),
                });

                location.reload();
            } catch (err) {
                console.log(err);
                alert('Failed apply changes');
            }
        }
    }

    async function edit_board_contents() {
        try {
            await pocketbase.collection('boards').update(id, {
                name: editBoardName(),
                description: editBoardDescription(),
            });

            location.reload();
        } catch (err) {
            console.log(err);
            alert('Failed apply changes');
        }
    }

    async function delete_board() {
        if (confirm('Are you sure you want to delete this board?')) {
            try {
                await pocketbase.collection('boards').delete(id);

                location.href = '/';
            } catch (error) {
                console.log(error);
                alert('Failed to delete board');
            }
        }
    }

    (async () => {
        try {
            let board_data = await pocketbase.collection('boards').getOne(id, {
                expand: 'members',
            });

            if (
                !(board_data.members as string[]).includes(
                    pocketbase.authStore.model.id
                )
            ) {
                setBoardData(null);
                setLoading(false);
                return;
            }

            const board_posts = await pocketbase
                .collection('posts')
                .getFullList({
                    filter: `(parent_board = '${board_data.id}')`,
                    expand: 'author',
                });

            setBoardPosts(board_posts);

            setEditBoardName(board_data.name);
            setEditBoardDescription(board_data.description);

            board_data['description'] = await markdownToHtml(
                board_data.description
            );

            setBoardData(board_data);
            setLoading(false);
        } catch (err) {
            console.log(err);
            setBoardData(null);
            setLoading(false);
        }
    })();
    return (
        <>
            <Nav />

            <div class="container mx-auto my-auto px-10 py-20">
                <Show when={isLoading() == true}>
                    <div class="flex justify-center items-center h-screen flex-col">
                        <span class="loading loading-spinner text-primary"></span>
                        Loading..
                    </div>
                </Show>
                <Show when={isLoading() == false}>
                    <Show when={boardData() == null}>
                        <div class="bg-red-400 p-2 rounded-md">
                            <h2 class="inline-flex items-center text-lg font-semibold">
                                <span class="pr-1">
                                    <IoWarningOutline />
                                </span>{' '}
                                Failed to load board data
                            </h2>
                            <p>
                                This board does not exist or you don't have
                                permission to view.
                            </p>
                        </div>
                    </Show>
                    <Show when={boardData() !== null}>
                        <title>Board - {boardData().name}</title>
                        <div class="flex space-x-0 md:space-x-4 flex-col sm:flex-row">
                            <div class="flex w-full">
                                <div class="mt-4 w-full bg-base-200 rounded-md py-4 px-5">
                                    <Show when={editMode() == true}>
                                        <input
                                            class="input w-full"
                                            value={editBoardName()}
                                            onInput={(e) =>
                                                setEditBoardName(
                                                    e.currentTarget.value
                                                )
                                            }
                                        />
                                        <textarea
                                            class="mt-4 textarea textarea-bordered textarea-lg w-full h-32"
                                            value={editBoardDescription()}
                                            onInput={(e) =>
                                                setEditBoardDescription(
                                                    e.currentTarget.value
                                                )
                                            }
                                        />
                                        <button
                                            class="my-4 w-full btn shadown-none hover:btn-success focus:btn-success"
                                            onclick={edit_board_contents}
                                        >
                                            <VsSaveAs /> Save
                                        </button>
                                    </Show>
                                    <Show when={editMode() == false}>
                                        <h2 class="font-semibold text-2xl capitalize">
                                            {boardData().name}
                                        </h2>
                                        <div
                                            class="markdown"
                                            innerHTML={boardData().description}
                                        ></div>
                                    </Show>
                                </div>
                            </div>
                            <div class="flex-1">
                                <div class="mt-4 bg-base-200 rounded-md py-4 px-8">
                                    <h2 class="font-semibold text-xl">
                                        Members
                                    </h2>
                                    <div class="mt-2">
                                        <For each={boardData().expand.members}>
                                            {(member) => (
                                                <div class="flex items-center space-x-2 mb-1">
                                                    <img
                                                        class="rounded-full h-10 w-10 max-w-none"
                                                        src={
                                                            pocketbase.files.getUrl(
                                                                member,
                                                                member.avatar
                                                            ) || '/default.jpg'
                                                        }
                                                    />
                                                    <span class="inline-flex items-center">
                                                        {member.username}
                                                        {boardData().owner ==
                                                            member.id && (
                                                            <TbCrown class="ml-1" />
                                                        )}
                                                    </span>
                                                    {boardData().owner ==
                                                        pocketbase.authStore
                                                            .model.id &&
                                                        member.id !==
                                                            pocketbase.authStore
                                                                .model.id && (
                                                            <div class="pl-4">
                                                                <button
                                                                    class="btn shadow-none btn-sm hover:btn-error focus:btn-error"
                                                                    onClick={(
                                                                        e
                                                                    ) =>
                                                                        remove_user(
                                                                            member.id
                                                                        )
                                                                    }
                                                                >
                                                                    <IoClose />
                                                                </button>
                                                            </div>
                                                        )}
                                                </div>
                                            )}
                                        </For>
                                        <Show
                                            when={
                                                boardData().owner ==
                                                    pocketbase.authStore.model
                                                        .id &&
                                                isUserPopupOpen() == false
                                            }
                                        >
                                            <button
                                                class="my-2 w-full btn btn-md shadown-none hover:btn-success focus:btn-success"
                                                onClick={add_user_open}
                                            >
                                                <FaSolidPlus /> Add
                                            </button>
                                        </Show>
                                        <Show
                                            when={
                                                boardData().owner ==
                                                    pocketbase.authStore.model
                                                        .id &&
                                                isUserPopupOpen() == true
                                            }
                                        >
                                            <div class="my-4">
                                                <MultiSelectUsers
                                                    users={userList()}
                                                    setSelectedUsersParent={
                                                        setMultiSelectItems
                                                    }
                                                />
                                            </div>
                                            <button
                                                class="my-1 w-full btn shadown-none hover:btn-success focus:btn-success"
                                                onClick={add_user_save}
                                            >
                                                <VsSaveAs /> Save
                                            </button>
                                            <button
                                                class="my-1 w-full btn shadown-none hover:btn-success focus:btn-success"
                                                onClick={() =>
                                                    setUserPopupStatus(false)
                                                }
                                            >
                                                <IoClose /> Exit
                                            </button>
                                        </Show>
                                    </div>
                                </div>
                                {boardData().owner ==
                                    pocketbase.authStore.model.id && (
                                    <div class="mt-4">
                                        <div>
                                            <button
                                                class="my-1 w-full btn shadown-none hover:btn-warning focus:btn-warning"
                                                onClick={() =>
                                                    setEditMode(!editMode())
                                                }
                                            >
                                                <BiRegularPencil /> Toggle edit
                                                mode
                                            </button>
                                            <button
                                                class="my-1 w-full btn shadown-none hover:btn-error focus:btn-error"
                                                onclick={delete_board}
                                            >
                                                <HiOutlineTrash /> Delete Board
                                            </button>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div class="mt-4 w-full bg-base-200 rounded-md py-4 px-5">
                            <div class="flex flex-wrap items-center justify-between">
                                <h2 class="font-semibold text-xl">Contents</h2>
                                <div>
                                    <a
                                        href={`/write/${boardData().id}`}
                                        class="btn hover:btn-primary focus:btn-primary"
                                    >
                                        <FaSolidPlus /> New
                                    </a>
                                </div>
                            </div>
                            <div class="mt-4">
                                <For each={boardPosts()}>
                                    {(post) => (
                                        <a
                                            href={`/${boardData().id}/${
                                                post.id
                                            }`}
                                        >
                                            <div class="my-4 bg-base-300 p-4 rounded-md shadow-md">
                                                <div class="mb-1 text-sm">
                                                    {format(
                                                        parseISO(post.updated),
                                                        'dd, MMM yy'
                                                    )}
                                                </div>
                                                <h2 class="font-semibold capitalize">
                                                    {post.title}
                                                </h2>
                                                {post.tags.length > 0 && (
                                                    <div class="space-x-1">
                                                        {post.tags
                                                            .split(',')
                                                            .map((tag) => (
                                                                <span class="badge badge-outline badge-primary">
                                                                    {tag.trim()}
                                                                </span>
                                                            ))}
                                                    </div>
                                                )}
                                                <div class="flex items-center space-x-2 mt-2">
                                                    <img
                                                        class="rounded-full h-8 w-8 max-w-none"
                                                        src={
                                                            pocketbase.files.getUrl(
                                                                post.expand
                                                                    .author,
                                                                post.expand
                                                                    .author
                                                                    .avatar
                                                            ) || '/default.jpg'
                                                        }
                                                    />
                                                    <span>
                                                        {
                                                            post.expand.author
                                                                .username
                                                        }
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    )}
                                </For>
                            </div>
                        </div>
                    </Show>
                </Show>
            </div>
        </>
    );
};

export default Boards;
