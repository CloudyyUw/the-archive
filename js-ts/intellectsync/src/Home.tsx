import {
    Component,
    For,
    Show,
    createEffect,
    createSignal,
    lazy,
} from 'solid-js';
import pocketbase from './utils/dbConnection';

import Nav from './components/nav';
import { FaSolidPlus } from 'solid-icons/fa';
import { BsPersonFill } from 'solid-icons/bs';
import { IoClose } from 'solid-icons/io';

const MultiSelectUsers = lazy(() => import('./components/multiSelectUsers'));

const Home: Component = () => {
    if (pocketbase.authStore.token.length > 0 == false) {
        location.href = '/login';
    }

    const [isLoading, setLoading] = createSignal(true),
        [boardData, setBoardData] = createSignal([]),
        [userList, setUserList] = createSignal([]),
        [getMultiSelectItems, setMultiSelectItems] = createSignal([]),
        [isPopupOpen, setPopupStatus] = createSignal(false);

    // Inputs
    const [boardName, setBoardName] = createSignal(''),
        [boardDescription, setBoardDescription] = createSignal('');

    async function create_board() {
        if (boardName() == '') {
            alert('Please provide a valid board name.');
            return;
        }

        try {
            const created_board = await pocketbase.collection('boards').create({
                name: boardName(),
                description: boardDescription(),
                owner: pocketbase.authStore.model.id,
                members: [pocketbase.authStore.model.id].concat(
                    getMultiSelectItems().map((u) => u.id) || []
                ),
            });

            location.href = `/board/${created_board.id}`;
        } catch (err) {
            alert('Failed to create board.');
            console.log(err);
            return;
        }
    }

    function open_or_close_popup() {
        setPopupStatus(!isPopupOpen());
    }

    createEffect(async () => {
        const query = await pocketbase.collection('boards').getFullList({
                filter: `(members:each ~ '${pocketbase.authStore.model.id}')`,
            }),
            users = await pocketbase.collection('users').getFullList({
                filter: `(id != '${pocketbase.authStore.model.id}')`,
            });
        setBoardData(query);
        setUserList(users);
        setLoading(false);
    });
    return (
        <>
            <title>Home</title>
            <Nav />
            <div class="container mx-auto my-auto px-10 py-20">
                <div class="flex flex-wrap items-center justify-between">
                    <h2 class="capitalize font-medium text-2xl">
                        Welcome {pocketbase.authStore.model.username}
                    </h2>
                    <div>
                        <button
                            class="btn hover:btn-primary"
                            onclick={open_or_close_popup}
                        >
                            <FaSolidPlus /> New board
                        </button>
                    </div>
                </div>
                {/* popup window */}
                <div
                    class={`${
                        isPopupOpen() == false ? 'hidden' : 'visible'
                    } fixed inset-0 flex items-center justify-center z-50`}
                >
                    <div class="fixed inset-0 bg-black opacity-50"></div>
                    <div class="bg-base-100 p-4 rounded-md shadow-lg z-10">
                        <Show when={isLoading() == true}>
                            <div class="flex justify-center items-center flex-col">
                                <span class="loading loading-spinner text-primary"></span>
                                Loading..
                            </div>
                        </Show>
                        <Show when={isLoading() == false}>
                            <div class="flex flex-wrap items-center justify-between space-x-4 border-b-2">
                                <div>
                                    <h2 class="text-lg">Create new board</h2>
                                </div>
                                <div>
                                    <button
                                        class="btn btn-sm btn-ghost"
                                        onclick={open_or_close_popup}
                                    >
                                        <IoClose size={20} />
                                    </button>
                                </div>
                            </div>
                            <div>
                                <div class="mt-2 form-control w-full">
                                    <label class="label">
                                        <span class="label-text">
                                            Name
                                            <span class="text-red-500">*</span>
                                        </span>
                                    </label>
                                    <input
                                        type="text"
                                        class="input input-bordered w-full max-w-xs"
                                        value={boardName()}
                                        onInput={(e) =>
                                            setBoardName(e.currentTarget.value)
                                        }
                                    />
                                    <label class="label">
                                        <span class="label-text">
                                            Description
                                            <br />
                                            <span class="italic">
                                                Supports markdown!
                                            </span>
                                        </span>
                                    </label>
                                    <textarea
                                        class="textarea textarea-bordered"
                                        value={boardDescription()}
                                        onInput={(e) =>
                                            setBoardDescription(
                                                e.currentTarget.value
                                            )
                                        }
                                    />
                                    <label class="label">
                                        <span class="label-text">Members</span>
                                    </label>
                                    <MultiSelectUsers
                                        setSelectedUsersParent={
                                            setMultiSelectItems
                                        }
                                        users={userList().map((user) => {
                                            return {
                                                id: user.id,
                                                username: user.username,
                                                avatar:
                                                    pocketbase.files.getUrl(
                                                        user,
                                                        user.avatar
                                                    ) || '/default.jpg',
                                            };
                                        })}
                                    />
                                    <button
                                        class="mt-2 btn btn-secondary hover:btn-primary focus:btn-primary"
                                        onclick={create_board}
                                    >
                                        Create
                                    </button>
                                </div>
                            </div>
                        </Show>
                    </div>
                </div>
                {/*  */}
                <div class="mt-4 bg-base-200 rounded-md py-4 px-5">
                    <Show when={isLoading() == true}>
                        <div class="flex justify-center items-center h-screen flex-col">
                            <span class="loading loading-spinner text-primary"></span>
                            Loading..
                        </div>
                    </Show>

                    <Show when={isLoading() == false}>
                        <h3 class="font-semibold text-xl">Boards</h3>
                        {boardData().length == 0 && (
                            <div class="mt-2">
                                Looks like you aren't in any board.
                            </div>
                        )}
                        {boardData().length > 0 && (
                            <For each={boardData()}>
                                {(board) => (
                                    <a href={`/board/${board.id}`}>
                                        <div class="my-3 bg-base-100 p-2 rounded-md shadow-sm">
                                            <h3 class="capitalize text-lg font-bold">
                                                {board.name}
                                            </h3>
                                            <div class="space-x-2">
                                                {board.owner ==
                                                    pocketbase.authStore.model
                                                        .id && (
                                                    <span class="badge badge-primary">
                                                        Owner
                                                    </span>
                                                )}
                                                <span class="badge inline-flex items-center">
                                                    <span class="pr-1">
                                                        <BsPersonFill />
                                                    </span>
                                                    {board.members.length}{' '}
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                )}
                            </For>
                        )}
                    </Show>
                </div>
            </div>
        </>
    );
};

export default Home;
