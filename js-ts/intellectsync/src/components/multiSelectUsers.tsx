import { Component, Setter, createSignal } from 'solid-js';

const MultiSelect: Component<{
    users: {
        id: string;
        username: string;
        avatar: string;
    }[];
    setSelectedUsersParent: Setter<any[]>;
}> = ({ users, setSelectedUsersParent }) => {
    const [isOpen, setOpen] = createSignal(false),
        [selectedUsers, setSelectedUsers] = createSignal([]);
    console.log(users);
    function toggle_popup() {
        setOpen(!isOpen());
    }

    function toggle_option(option) {
        if (selectedUsers().includes(option)) {
            setSelectedUsers(
                selectedUsers().filter((selected) => selected !== option)
            );
        } else {
            setSelectedUsers([...selectedUsers(), option]);
        }

        setSelectedUsersParent(selectedUsers());
    }
    return (
        <div class="relative">
            <input
                type="text"
                class="input input-bordered w-full max-w-xs"
                value={selectedUsers()
                    .map((u) => u.username)
                    .join(', ')}
                placeholder="Select users"
                readonly
                onClick={toggle_popup}
            />

            {isOpen() && (
                <div class="absolute w-full min-w-max overflow-y-auto max-h-52 md:max-h-36 bg-base-100 p-4 rounded-md shadow-md">
                    {users.map((option) => (
                        <div class="flex justify-between mb-1">
                            <div class="">
                                <img
                                    class="avatar rounded-full h-10 w-10"
                                    src={option.avatar}
                                />
                                <span class="pl-4">{option.username}</span>
                            </div>
                            <input
                                type="checkbox"
                                class="checkbox ml-4"
                                checked={selectedUsers().includes(option)}
                                onChange={() => toggle_option(option)}
                            />
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default MultiSelect;
