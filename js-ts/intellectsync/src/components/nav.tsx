import { Component, Show } from 'solid-js';
import pocketbase from '../utils/dbConnection';

const Nav: Component = () => {
    function sign_out() {
        pocketbase.authStore.clear();
        location.href = '/login';
    }

    return (
        <div class="navbar fixed backdrop-blur-sm bg-opacity-20 bg-base-100">
            <div class="flex-1">
                <a class="btn btn-ghost text-xl" href="/">
                    {import.meta.env.VITE_APPNAME}
                </a>
            </div>
            <div class="dropdown dropdown-end">
                <label tabIndex={0} class="btn btn-ghost btn-circle avatar">
                    <div class="w-10 rounded-full">
                        <img
                            src={
                                pocketbase.files.getUrl(
                                    pocketbase.authStore.model,
                                    pocketbase.authStore.model.avatar
                                ) || '/default.jpg'
                            }
                        />
                    </div>
                </label>
                <ul
                    tabIndex={0}
                    class="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52"
                >
                    <li>
                        <a
                            class="hover:bg-primary hover:bg-opacity-50"
                            href="/me"
                        >
                            Profile
                        </a>
                    </li>
                    <li>
                        <a
                            class="hover:bg-primary hover:bg-opacity-50"
                            onclick={sign_out}
                        >
                            Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default Nav;
