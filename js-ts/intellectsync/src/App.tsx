import type { Component } from 'solid-js';
import { lazy } from 'solid-js';
import { Routes, Route } from '@solidjs/router';

const Login = lazy(() => import('./Login')),
    Home = lazy(() => import('./Home')),
    Boards = lazy(() => import('./Boards')),
    Write = lazy(() => import('./Write')),
    Post = lazy(() => import('./Post'));

const App: Component = () => {
    return (
        <>
            <Routes>
                <Route path="/" component={Home} />
                <Route path="/login" component={Login} />
                <Route path="/board/:id" component={Boards} />
                <Route path="/:board_id/:post_id" component={Post} />
                <Route path="/write/:id" component={Write} />
            </Routes>
        </>
    );
};

export default App;
