import { Component, Show, createSignal } from 'solid-js';
import pocketbase from './utils/dbConnection';

const Login: Component = () => {
    if (pocketbase.authStore.token.length > 0) {
        location.href = '/';
    }

    const [username, setUsername] = createSignal(''),
        [passwd, setPass] = createSignal(''),
        [warning, setWarning] = createSignal('');

    async function auth_register() {
        try {
            console.log(
                await pocketbase.collection('users').create({
                    username: username(),
                    password: passwd(),
                    passwordConfirm: passwd(),
                })
            );

            setWarning('Account created, you can log in now!');
        } catch (err) {
            console.log(err);
            setWarning(`Failed to register a new account.`);
        }
    }
    async function auth_login() {
        try {
            await pocketbase
                .collection('users')
                .authWithPassword(username(), passwd());

            location.href = '/';
        } catch (err) {
            console.log(err);
            setWarning(
                `Failed to log in. Check the credentials and try again.`
            );
        }
    }
    return (
        <>
            <title>Login</title>
            <div class="flex mt-32 justify-center items-center">
                <div class="m-auto">
                    <div>
                        <h1 class="text-center font-semibold text-3xl bg-clip-text text-transparent bg-gradient-to-r from-primary to-secondary">
                            Welcome
                        </h1>
                        <p>Login or create an account to continue</p>
                    </div>
                    <Show when={warning().length > 0}>
                        <div class="bg-accent-content rounded-md p-2 my-2">
                            {warning()}
                        </div>
                    </Show>
                    <div class="mt-1 form-control w-full border rounded-md shadow-md p-4">
                        <label class="label">
                            <span class="label-text">Username</span>
                        </label>
                        <input
                            type="text"
                            class="input input-bordered w-full max-w-xs"
                            value={username()}
                            onInput={(e) => setUsername(e.currentTarget.value)}
                        />

                        <label class="label">
                            <span class="label-text">Password</span>
                        </label>
                        <input
                            type="password"
                            class="input input-bordered w-full max-w-xs"
                            value={passwd()}
                            onInput={(e) => setPass(e.currentTarget.value)}
                        />
                        <button
                            class="mt-2 btn btn-secondary hover:btn-primary focus:btn-primary"
                            onclick={auth_login}
                        >
                            Login
                        </button>
                        <button
                            class="mt-2 btn btn-secondary hover:btn-primary focus:btn-primary"
                            onclick={auth_register}
                        >
                            Register
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Login;
