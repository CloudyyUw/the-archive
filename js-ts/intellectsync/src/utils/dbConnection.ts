import PocketBase from 'pocketbase';

const pb_conn = new PocketBase(import.meta.env.VITE_DATABASE_URL);

if (pb_conn.authStore.token) await pb_conn.collection('users').authRefresh();

export default pb_conn;
