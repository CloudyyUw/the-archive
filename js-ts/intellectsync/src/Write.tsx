import { Component, createSignal, Show, createEffect } from 'solid-js';
import { useParams } from '@solidjs/router';
import pocketbase from './utils/dbConnection';
import markdownToHtml from './utils/markdownToHtml';

import Nav from './components/nav';
import { VsSaveAs } from 'solid-icons/vs';
import { FaSolidPlus } from 'solid-icons/fa';

import './css/markdown.css';

const Write: Component = () => {
    if (pocketbase.authStore.token.length > 0 == false) {
        location.href = '/login';
    }

    const { id } = useParams(),
        [currentTab, setCurrentTab] = createSignal('edit'),
        [postTitle, setPostTitle] = createSignal(''),
        [postTags, setPostTags] = createSignal(''),
        [postContent, setPostContent] = createSignal(''),
        [htmlContent, setHtmlContent] = createSignal(''),
        [files, setFiles] = createSignal([]);

    async function update_html_content() {
        const html = await markdownToHtml(postContent());
        setHtmlContent(html);
    }

    function handleFileChanges(i, e) {
        const filesList = [...files()];

        filesList[i] = e.target.files[0];

        setFiles(filesList);
    }

    function handleAddButtonClick() {
        const newFiles = [...files(), null];
        setFiles(newFiles);
    }

    function handleInsertImage(index) {
        const filename = files()[index]?.name;
        if (filename) {
            setPostContent((prevContent) => prevContent + `![](f$${filename})`);
        }
    }

    async function saveData() {
        try {
            if (postContent() !== '' && postTitle() !== '') {
                const formData = new FormData();

                formData.append('parent_board', id);
                formData.append('author', pocketbase.authStore.model.id);
                formData.append('title', postTitle());
                formData.append('content', postContent());
                formData.append('tags', postTags());

                files().forEach((file) => {
                    if (file) {
                        formData.append(`attachments`, file);
                    }
                });

                pocketbase
                    .collection('posts')
                    .create(formData)
                    .then((created_post) => {
                        location.href = `/${id}/${created_post.id}`;
                    });
            }
        } catch (error) {
            console.log(error);
            alert('Failed apply changes');
        }
    }

    createEffect(() => {
        update_html_content();
    });

    return (
        <>
            <Nav />
            <title>Write</title>
            <div class="container mx-auto my-auto px-10 py-20">
                <div class="mt-4 w-full bg-base-200 rounded-md py-4 px-5 ">
                    <div role="tablist" class="tabs tabs-bordered mb-2">
                        <a
                            role="tab"
                            class={`tab ${
                                currentTab() == 'edit' ? 'tab-active' : ''
                            }`}
                            onclick={() => setCurrentTab('edit')}
                        >
                            Edit
                        </a>
                        <a
                            role="tab"
                            class={`tab ${
                                currentTab() == 'view' ? 'tab-active' : ''
                            }`}
                            onclick={() => setCurrentTab('view')}
                        >
                            Preview
                        </a>
                    </div>
                    <Show when={currentTab() == 'edit'}>
                        <input
                            class="input w-full"
                            placeholder="Title"
                            value={postTitle()}
                            onInput={(e) => setPostTitle(e.currentTarget.value)}
                        />
                        <input
                            class="input w-full mt-2"
                            placeholder="Tag1, Tag2..."
                            value={postTags()}
                            onInput={(e) => setPostTags(e.currentTarget.value)}
                        />
                        <textarea
                            class="mt-4 textarea textarea-bordered textarea-lg w-full h-32"
                            placeholder="Content"
                            value={postContent()}
                            onInput={(e) =>
                                setPostContent(e.currentTarget.value)
                            }
                        />
                        <div class="mt-4">
                            <div class="inline-flex items-center space-x-2">
                                <h3 class="font-semibold text-lg">
                                    Attachments
                                </h3>
                                <button
                                    class="btn btn-sm"
                                    onClick={handleAddButtonClick}
                                >
                                    <FaSolidPlus /> Add
                                </button>
                            </div>
                            {files().map((file, index) => (
                                <div class="mt-3">
                                    <div class="inline-flex items-center space-x-2">
                                        <label
                                            for={`finput-${index}`}
                                            class="btn btn-secondary"
                                        >
                                            Select File
                                        </label>
                                        <input
                                            id={`finput-${index}`}
                                            type="file"
                                            class="hidden"
                                            onChange={(e) =>
                                                handleFileChanges(index, e)
                                            }
                                        />
                                        <span>
                                            {file
                                                ? file.name
                                                : 'No file selected'}
                                        </span>
                                        <button
                                            class="btn"
                                            onClick={() =>
                                                handleInsertImage(index)
                                            }
                                        >
                                            Insert
                                        </button>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </Show>
                    <Show when={currentTab() == 'view'}>
                        <div class="mt-4">
                            <h2 class="capitalize text-xl font-semibold">
                                {postTitle()}
                            </h2>
                            <div class="space-x-1">
                                {postTags()
                                    .split(',')
                                    .map((tag) => (
                                        <span class="badge badge-outline badge-primary">
                                            {tag.trim()}
                                        </span>
                                    ))}
                            </div>
                            <div class="mb-2">
                                <div
                                    innerHTML={htmlContent()}
                                    class="markdown"
                                ></div>
                            </div>
                        </div>
                    </Show>
                    <div class="flex items-end justify-end mt-10">
                        <button
                            class="btn hover:btn-success focus:btn-success"
                            onclick={saveData}
                        >
                            <VsSaveAs /> Save
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Write;
