use std::{io::Write, net::TcpStream, str::from_utf8};

pub fn parse_input(stream: &mut TcpStream, input: [u8; 1048576], ramdb: &mut crate::RamDB) {
    let input_as_str = from_utf8(&input).unwrap();

    if input_as_str.starts_with("disconnect;") {
        stream.write(b"Bye!").unwrap();
        stream.shutdown(std::net::Shutdown::Both).unwrap();
        return;
    }

    match input_as_str
        .strip_suffix("\r\n")
        .or(input_as_str.strip_prefix("\n"))
        .unwrap_or(input_as_str)
        .split(" ")
        .collect::<Vec<&str>>()[0]
    {
        // insert "value" at "index";
        "insert" => {
            let insert_value = &input_as_str[input_as_str.find("\"").unwrap_or(0)
                ..input_as_str.find(" at").unwrap_or(input_as_str.len())];
            let insert_index = &input_as_str[input_as_str.find("at \"").unwrap_or(0) + 3 // trim "at "
            ..input_as_str.find(";").unwrap_or(input_as_str.len())];
            let parsed_index = &insert_index[1..insert_index.len() - 1]; // trim "
            let parsed_value = &insert_value[1..insert_value.len() - 1];

            ramdb.insert(parsed_index.to_string(), parsed_value.to_string());

            stream.write(b"OK").unwrap();
        }
        // get "index";
        "get" => {
            let index_value = &input_as_str
                [input_as_str.find("\"").unwrap_or(0)..input_as_str.find(";").unwrap_or(0)];
            let parsed_index = &index_value[1..index_value.len() - 1];

            match ramdb.get(parsed_index.to_string()) {
                Some(val) => stream.write(val.as_bytes()).unwrap(),
                None => stream.write(b"").unwrap(),
            };
        }
        // has "index";
        "has" => {
            let index_value = &input_as_str
                [input_as_str.find("\"").unwrap_or(0)..input_as_str.find(";").unwrap_or(0)];
            let parsed_index = &index_value[1..index_value.len() - 1];

            stream
                .write(
                    ramdb
                        .has_key(parsed_index.to_string())
                        .to_string()
                        .as_bytes(),
                )
                .unwrap();
        }
        // edit "value" at "index";
        "edit" => {
            let insert_value = &input_as_str[input_as_str.find("\"").unwrap_or(0)
                ..input_as_str.find(" at").unwrap_or(input_as_str.len())];
            let insert_index = &input_as_str[input_as_str.find("at \"").unwrap_or(0) + 3 // trim "at "
            ..input_as_str.find(";").unwrap_or(input_as_str.len())];
            let parsed_index = &insert_index[1..insert_index.len() - 1]; // trim "
            let parsed_value = &insert_value[1..insert_value.len() - 1];

            ramdb.edit(parsed_index.to_string(), parsed_value.to_string());

            stream.write(b"OK").unwrap();
        }
        // remove "index";
        "remove" => {
            let index_value = &input_as_str
                [input_as_str.find("\"").unwrap_or(0)..input_as_str.find(";").unwrap_or(0)];
            let parsed_index = &index_value[1..index_value.len() - 1];

            ramdb.drop(parsed_index.to_string());

            stream.write(b"OK").unwrap();
        }
        _ => (),
    }
}
